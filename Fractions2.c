#include<stdio.h>
struct fractions
{
	int n;
	int d;

};
typedef struct fractions Fra;

Fra input()
{
	Fra f;
	printf("enter the numerator \n");
	scanf("%d",&f.n);
	printf("enter the denominator \n");
	scanf("%d",&f.d);
	return f;
}
Fra add(int n,Fra s[n])
{
	Fra sum;
	sum.n=s[1].n;
	sum.d=s[1].d;
	for(int i=2;i<=n;i++)
	{
		sum.n=(s[i].d*sum.n) + (s[i].n*sum.d);
		sum.d=(s[i].d+sum.d);
	}
	return sum;
}
void display(int n,Fra sum)
{
	printf("the sum of the %d fractions  is %d/%d \n",n,sum.n,sum.d);
}
int main()
{
	int n;
	Fra sum;
	printf("enter the value of n \n");
	scanf("%d",&n);
	Fra s[n];
	for(int i=1;i<=n;i++)
	{

		s[i]=input();
	}

	sum=add(n,s);
	int hcf=1;
    for (int i=1;i<=(sum.n * sum.d);i++)
    {
        if ( (sum.n)%i == 0 && (sum.d )% i== 0)
        {
            hcf=i;
        }
    }
    sum.n=sum.n/hcf;
    sum.d=sum.d/hcf;
	display(n,sum);
	return(0);
}
