#include<stdio.h>
struct fractions
{
    int n;
    int d;
};
typedef struct fractions Fra;

Fra input()
{
    Fra f;
    printf("enter the numerator \n");
    scanf("%d",&f.n);
    printf("enter the denominator \n");
    scanf("%d",&f.d);
    return f;
}
Fra add(Fra f1,Fra f2)
{
    Fra sum;
    sum.n=(f1.n*f2.d) + (f2.n*f1.d);
    sum.d=(f1.d)*(f2.d);
    return sum;
}
void display(Fra f1,Fra f2,Fra sum)
{
    printf("the sum of the two fractions %d/%d and %d/%d is %d/%d \n",f1.n,f1.d,f2.n,f2.d,sum.n,sum.d);
}
int main()
{
    Fra f1,f2,sum;
    f1=input();
    f2=input();
    sum=add(f1,f2);
    int hcf=1;
    for (int i=1;i<=(sum.n * sum.d);i++)
    {
        if ( (sum.n)%i == 0 && (sum.d )% i== 0)
        {
            hcf=i;
        }
    }
    sum.n=sum.n/hcf;
    sum.d=sum.d/hcf;
    display(f1,f2,sum);
    return(0);
}
