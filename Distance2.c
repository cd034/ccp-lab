#include<stdio.h>
#include<math.h>
struct point
{
	float x;
	float y;
};
typedef struct point Point;
Point input()
{
	Point p;
	printf("enter the x coordinate\n");
	scanf("%f",&p.x);
	printf("enter the y coordinate\n");
	scanf("%f",&p.y);
	return p;
}
float distance(Point p1,Point p2)
{
	float d;
	d=sqrt(pow((p1.x-p2.x),2)-pow((p1.y-p2.y),2));
	return d;
}
void display(Point p1,Point p2,float d)
{
	printf("distance between %f,%f and %f,%f is %f\n",p1.x,p1.y,p2.x,p2.y,d);
}
int main()
{
	Point p1,p2;
	p1=input();
	p2=input();
	float d;
	d=distance(p1,p2);
	display(p1,p2,d);
	return 0;
}


